module.exports = {

    searchRessource: function (convo, message, bot) {

        //*********************************
        // DEBUG!
        // THIS WILL THROW AN UNCAUGHT ERROR
        // DEBUG!
        // *********************************

        // try {
        //     var fs = require('fs');
        //
        //     fs.readFile('somefile.txt', function (err, data) {
        //         if (err) throw err;
        //         console.log(data);
        //     });
        // } catch (err) {
        //     require("../../util/errorUtil").displayErrorMessage(bot, message, convo, err, false, false);
        // }

        //*********************************
        // DEBUG!
        // THIS WILL THROW AN UNCAUGHT ERROR
        // DEBUG!
        //*********************************


        const {t} = require('../../node_modules/localizify');
        const logUtil = require("../../util/logUtil");

        convo.addMessage(t('ressource.searchRessource'));

        //********************************
        // Helpers
        //********************************
        // Import Helper Class to get Entites from LUIS Response
        const luisUtil = require("../../util/luisUtil");
        const errorUtil = require("../../util/errorUtil");

        //********************************
        // Required Threads
        //********************************
        const ressourceSkill = require("./requiredInformationen/ressourceSkill");
        const ressourceTechnology = require("./requiredInformationen/ressourceTechnology");
        const ressourceProject = require("./requiredInformationen/ressourceProject");
        const ressourceCustomer = require("./requiredInformationen/ressourceCustomer");
        const ressourceExperienceLevel = require("./requiredInformationen/ressourceExperienceLevel");
        const ressourceExperienceYears = require("./requiredInformationen/ressourceExperienceYears");

        const foundRessources = require('./foundRessource/foundRessource');

        //********************************
        // Initialize Conversation
        //********************************
        ressourceTechnology.displayTechnologys(bot, message, convo, luisUtil, "askInitialTechnology", "askInitialSkill");
        ressourceSkill.displaySkills(bot, message, convo, luisUtil, "askInitialSkill", "askInitialProject0");
        ressourceProject.displayProjects(bot, message, convo, luisUtil, "askInitialProject", "askInitialCustomer");
        ressourceCustomer.displayCustomers(bot, message, convo, luisUtil, "askInitialCustomer", "askInitialExperienceLevel");
        ressourceExperienceLevel.displayExperienceLevels(bot, message, convo, luisUtil, "askInitialExperienceLevel", "askInitialExperienceYears");
        ressourceExperienceYears.displayExperienceYears(bot, message, convo, luisUtil, "askInitialExperienceYears", "requiredInfromationMenu");

        ressourceSkill.displaySkills(bot, message, convo, luisUtil, "askSkill_reqMenu", "requiredInfromationMenu");
        ressourceTechnology.displayTechnologys(bot, message, convo, luisUtil, "askTechnology_reqMenu", "requiredInfromationMenu");
        ressourceProject.displayProjects(bot, message, convo, luisUtil, "askProject_reqMenu", "requiredInfromationMenu");
        ressourceCustomer.displayCustomers(bot, message, convo, luisUtil, "askCustomer_reqMenu", "requiredInfromationMenu");
        ressourceExperienceLevel.displayExperienceLevels(bot, message, convo, luisUtil, "askExperienceLevel_reqMenu", "requiredInfromationMenu");
        ressourceExperienceYears.displayExperienceYears(bot, message, convo, luisUtil, "askExperienceYears_reqMenu", "requiredInfromationMenu");

        try {
            //Variables required to search for a ressource
            let aVars = ["ressourceSkill", "ressourceTechnology", "ressourceProject", "ressourceCustomer", "ressourceExperienceLevel", "ressourceExperienceYears"];

            for (var x = 0; x < aVars.length; x++) {

                let aEntity = luisUtil.getEntityFromLuisResponse(aVars[x], message);

                if (aEntity === null || aEntity === undefined || aEntity.length === 0) {
                    logUtil.debug("Initialize var " + aVars[x] + " with value to 'None'");
                    convo.setVar(aVars[x], "None");
                } else {
                    if (aEntity[1]) {
                        convo.setVar(aVars[x], aEntity[1]);
                        logUtil.debug(aVars[x] + " = " + aEntity[1]);
                    } else {
                        convo.setVar(aVars[x], aEntity[0]);
                        logUtil.debug(aVars[x] + " = " + aEntity[0]);
                    }
                }
            }

            //Max results to display from DB
            convo.setVar("limitSkill", 5);
            convo.setVar("limitTechnology", 5);
            convo.setVar("limitProject", 5);
            convo.setVar("limitCustomer", 5);
            convo.setVar("limitExperienceLevel", 5);
            convo.setVar("limitEmployee", 3);

            //Offset for DB results
            convo.setVar("offsetSkill", 0);
            convo.setVar("offsetTechnology", 0);
            convo.setVar("offsetProject", 0);
            convo.setVar("offsetCustomer", 0);
            convo.setVar("offsetExperienceLevel", 0);
            convo.setVar("offsetEmployee", 0);

        } catch (err) {
            errorUtil.displayErrorMessage(bot, message, err, false, false);
        }

        //********************************
        //Conversation Threads
        //********************************
        convo.addQuestion({
            text: t('ressource.initialMessage_Question'),
            quick_replies: [
                {
                    title: t('yes'),
                    payload: t('yes'),
                },
                {
                    title: t('no'),
                    payload: t('no'),
                },
            ]
        }, [
            {
                default: true,
                callback: function (res, convo) {

                    try {
                        switch (res.text) {

                            case t('yes'):
                                convo.transitionTo("askInitialTechnology", t('ressource.aks_for_required_information'));
                                convo.next();
                                break;
                            case t('no'):
                                convo.gotoThread("helpMenu");
                                convo.next();
                                break;
                            default:
                                convo.say(t('did_not_understand'));
                                convo.repeat();
                                break;
                        }
                    } catch (err) {
                        errorUtil.displayErrorMessage(bot, message, err, false, false);
                    }

                }
            }
        ], {}, "initialMessage");


        convo.addMessage(t('ressource.NeccessaryInfromation', {
            ressourceSkill: "{{vars.ressourceSkill}}",
            ressourceTechnology: "{{vars.ressourceTechnology}}",
            ressourceProject: "{{vars.ressourceProject}}",
            ressourceCustomer: "{{vars.ressourceCustomer}}",
            ressourceExperienceLevel: "{{vars.ressourceExperienceLevel}}",

        }), "requiredInfromationMenu");

        convo.addQuestion({
            text: t('ressource.correctNeccessaryInfromation_Question'),
            quick_replies: [
                {
                    title: t('yes'),
                    payload: t('yes'),
                },
                {
                    title: t('ressource.correctNeccessaryInfromation_Question_Qr_No'),
                    payload: t('no'),
                },
            ]
        }, [
            {
                default: true,
                callback: function (res, convo) {
                    try {
                        switch (res.text) {

                            case t('yes'):
                                convo.gotoThread("displayFoundRessources");
                                break;
                            case t('no'):
                                convo.gotoThread("correctRequiredInfromation");
                                break;
                            default:
                                convo.say(t('did_not_understand'));
                                convo.repeat();
                                break;
                        }
                    } catch (err) {
                        errorUtil.displayErrorMessage(bot, message, err, false, false);
                    }

                }
            }
        ], {}, "requiredInfromationMenu");

        convo.addQuestion({
            text: t('ressource.correctNeccessaryInfromation_Question'),
            quick_replies: [
                {
                    title: t('ressource.skill'),
                    payload: t('ressource.skill'),
                },
                {
                    title: t('ressource.technology'),
                    payload: t('ressource.technology'),
                },
                {
                    title: t('ressource.experience_level'),
                    payload: t('ressource.experience_level'),
                },
                {
                    title: t('ressource.experience_years'),
                    payload: t('ressource.experience_years'),
                },
                {
                    title: t('ressource.customer'),
                    payload: t('ressource.customer'),
                },
                {
                    title: t('ressource.project'),
                    payload: t('ressource.project'),
                }
            ]
        }, [
            {
                default: true,
                callback: function (res, convo) {

                    try {
                        switch (res.text) {
                            case t('ressource.skill'):
                                convo.gotoThread("askSkill_reqMenu");
                                break;
                            case t('ressource.technology'):
                                convo.gotoThread("askTechnology_reqMenu");
                                break;
                            case t('ressource.experience_level'):
                                convo.gotoThread("askExperienceLevel_reqMenu");
                                break;
                            case t('ressource.experience_years'):
                                convo.gotoThread("askExperienceYears_reqMenu");
                                break;
                            case t('ressource.customer'):
                                convo.gotoThread("askCustomer_reqMenu");
                                break;
                            case t('ressource.project'):
                                convo.gotoThread("askProject_reqMenu");
                                break;
                            default:
                                convo.say(t('did_not_understand'));
                                convo.repeat();
                                break;
                        }
                    } catch (err) {
                        errorUtil.displayErrorMessage(bot, message, err, false, false);
                    }

                }
            }
        ], {}, "correctRequiredInfromation");


        //Start convo by asking Skill
        convo.gotoThread("initialMessage");

    }

};
