module.exports = {
    displaySkills: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const skillHelper = require('../../../util/helper/skillHelper');
        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");

        skillHelper.getSkillsFromDB(bot, message, convo, luisUtil, nextThread, function (conversation, rows) {

            logUtil.debug("All Skills to display in Convo: " + JSON.stringify(rows));

            if (rows.length === 0) {
                conversation.transitionTo(nextThread, t("ressource.askSkills.no_skills_found"));
            } else {

                let qr = [];

                qr.push({title: t("ressource.askSkills.no_skills_required"), payload: t("ressource.askSkills.no_skills_required")})

                for (let i = 0; i < rows.length; i++) {

                    let oRow = rows[i];

                    if (oRow.code != null && oRow.value !== "") {
                        qr.push({title: oRow.value, payload: oRow.value})
                    }
                }

                //If there are 5 elements, offer to display more.
                if (rows.length === 5) {
                    qr.push({title: t("ressource.askSkills.display_more_skills"), payload: t("ressource.askSkills.display_more_skills")})
                }

                conversation.addQuestion({
                    text: t("ressource.askSkills.found_skills"),
                    quick_replies: qr
                }, [
                    {
                        default: true,
                        callback: function (res, conversation) {

                            try {

                                if (res.text === t("ressource.askSkills.display_more_skills")) {

                                    // //Add +5 to current offset, to display more results from db
                                    // conversation.setVar("offsetSkill", conversation.vars.offsetSkill + 5);
                                    //
                                    // this.displaySkills(bot, message, conversation, luisUtil, nextThread)

                                    conversation.transitionTo(threadName, t('nicht_implementiert'));

                                } else if (res.text === t("ressource.askSkills.no_skills_required")) {

                                    //Set var in convo --> used afterwards to get search results form db
                                    conversation.setVar("ressourceSkill", "None");

                                    //Reset offset
                                    conversation.setVar("offsetSkill", 0);

                                    //continue to next thread
                                    if (nextThread !== "None") {
                                        conversation.gotoThread(nextThread);
                                    } else {
                                        conversation.next();
                                    }

                                } else {

                                    let aEntity = luisUtil.getEntityFromLuisResponse("ressourceSkill", res);

                                    if (aEntity === null || aEntity === undefined || aEntity.length === 0 || aEntity === "") {
                                        // array empty or does not exist
                                        conversation.transitionTo(threadName, t('did_not_understand'));
                                    } else {

                                        //Set var in convo --> used afterwards to get search results form db
                                        conversation.setVar("ressourceSkill", aEntity[0]);

                                        //Reset offset
                                        conversation.setVar("offsetSkill", 0);
                                        logUtil.debug("ressourceSkill = " + convo.vars.ressourceSkill);

                                        //continue to next thread
                                        if (nextThread !== "None") {
                                            conversation.gotoThread(nextThread);
                                        } else {
                                            conversation.next();
                                        }

                                    }

                                }

                            } catch (err) {
                                errorUtil.displayErrorMessage(bot, message, err, false, false);
                            }

                        }
                    }
                ], {}, threadName);

            }
        }, convo.vars.limitSkill, convo.vars.offsetSkill);


    },

};
