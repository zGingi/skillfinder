module.exports = {
    displayExperienceYears: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");

        let qr = [];

        qr.push({
            title: t("ressource.askExperienceYears.no_experience_levels_years_required"),
            payload: t("ressource.askExperienceYears.no_experience_levels_years_required")
        });

        qr.push({
            title: t("ressource.askExperienceYears.experience_levels_years_1_years"),
            payload: t("ressource.askExperienceYears.no_experience_levels_years_required")
        });
        qr.push({
            title: t("ressource.askExperienceYears.experience_levels_years_5_years"),
            payload: t("ressource.askExperienceYears.experience_levels_years_5_years")
        });
        qr.push({
            title: t("ressource.askExperienceYears.experience_levels_years_5+_years"),
            payload: t("ressource.askExperienceYears.experience_levels_years_5+_years")
        });

        convo.addQuestion({
            text: t("ressource.askExperienceYears.found_experience_levels_years"),
            quick_replies: qr
        }, [
            {
                default: true,
                callback: function (res, convo) {

                    try {

                        switch (res.text) {

                            case t("ressource.askExperienceYears.no_experience_levels_years_required"):
                                //Set var in convo --> used afterwards to get search results form db
                                convo.setVar("ressourceExperienceLevel", "None");
                                break;

                            case t("ressource.askExperienceYears.experience_levels_years_1_years"):
                                //Set var in convo --> used afterwards to get search results form db
                                convo.setVar("ressourceExperienceYears", "1");
                                break;

                            case t("ressource.askExperienceYears.experience_levels_years_5_years"):
                                //Set var in convo --> used afterwards to get search results form db
                                convo.setVar("ressourceExperienceYears", "5");
                                break;

                            case t("ressource.askExperienceYears.experience_levels_years_5+_years"):
                                //Set var in convo --> used afterwards to get search results form db
                                convo.setVar("ressourceExperienceYears", "5+");
                                break;

                            default:
                                convo.say(t('did_not_understand'));
                                convo.repeat();
                                break;

                        }

                        const foundRessources = require('../foundRessource/foundRessource');
                        foundRessources.displayFoundRessources(bot, message, convo, luisUtil, "displayFoundRessources", "displayFoundEmployee");

                        if (nextThread !== "None") {
                            convo.gotoThread(nextThread);
                        } else {
                            convo.next();
                        }

                    } catch (err) {
                        errorUtil.displayErrorMessage(bot, message, err, false, false);
                    }

                }
            }
        ], {}, threadName);


    }

};
