module.exports = {
    displayProjects: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const projectHelper = require('../../../util/helper/projectHelper');
        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");

        projectHelper.getProjectFromDB(bot, message, convo, luisUtil, nextThread, function (conversation, rows) {

            logUtil.debug("All Projects to display in Convo: " + JSON.stringify(rows));

            if (rows.length === 0) {
                conversation.transitionTo(nextThread, t("ressource.askProjects.no_projects_found"));
            } else {

                let maxQRToDisplay = convo.vars.limitProject;
                let offset = 0;

                for (let c = 0; c < Math.ceil(rows.length/maxQRToDisplay); c++){


                    let qr = [];
                    let counter = 0;

                    qr.push({title: t("ressource.askProjects.no_projects_required"), payload: t("ressource.askProjects.no_projects_required")});

                    //Add a max of 5 QR to the Question
                    for (let i = offset; i < rows.length; i++) {
                        if(counter < maxQRToDisplay){
                            let oRow = rows[i];
                            if (oRow.code != null && oRow.value !== "") {
                                qr.push({title: oRow.value, payload: oRow.value})
                            }
                        }
                        counter++;
                    }

                    //Updated offset
                    offset += maxQRToDisplay;

                    //If there are more thean "maxQRToDisplay" elements, offer to display more.
                    if (c >= 1) {
                        qr.push({title: t("back"), payload: t("back")})
                    }

                    //If there are more thean "maxQRToDisplay" elements, offer to display more.
                    if (counter >= maxQRToDisplay) {
                        qr.push({title: t("ressource.askProjects.display_more_projects"), payload: t("ressource.askProjects.display_more_projects")})
                    }

                    conversation.addQuestion({
                        text: t("ressource.askProjects.found_projects"),
                        quick_replies: qr
                    }, [
                        {
                            default: true,
                            callback: function (res, conversation) {

                                try {

                                    if (res.text === t("ressource.askProjects.display_more_projects")) {

                                        //Go to thread with next options
                                        conversation.transitionTo(threadName + (c+1), threadName + (c+1));

                                    }  else if (res.text === t("back")) {

                                        //Go to thread with previous options
                                        conversation.transitionTo(threadName + (c-1), threadName + (c-1));

                                    } else if (res.text === t("ressource.askProjects.no_projects_required")) {

                                        //Set var in convo --> used afterwards to get search results form db
                                        conversation.setVar("ressourceProject", "None");

                                        //Reset offset
                                        conversation.setVar("offsetProject", 0);

                                        //continue to next thread
                                        if (nextThread !== "None") {
                                            conversation.gotoThread(nextThread);
                                        } else {
                                            conversation.next();
                                        }

                                    } else {

                                        let aEntity = luisUtil.getEntityFromLuisResponse("ressourceProject", res);

                                        if (aEntity === null || aEntity === undefined || aEntity.length === 0 || aEntity === "") {
                                            // array empty or does not exist
                                            conversation.transitionTo(threadName, t('did_not_understand'));
                                        } else {

                                            //Set var in convo --> used afterwards to get search results form db
                                            conversation.setVar("ressourceProject", aEntity[0]);

                                            //Reset offset
                                            conversation.setVar("offsetProject", 0);
                                            logUtil.debug("ressourceProject = " + convo.vars.ressourceProject);

                                            //continue to next thread
                                            if (nextThread !== "None") {
                                                conversation.gotoThread(nextThread);
                                            } else {
                                                conversation.next();
                                            }

                                        }

                                    }

                                } catch (err) {
                                    errorUtil.displayErrorMessage(bot, message, err, false, false);
                                }

                            }
                        }
                    ], {}, threadName + c);

                }

            }
        });


    }

};
