module.exports = {
    displayTechnologys: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const technologyHelper = require('../../../util/helper/technologyHelper');
        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");

        technologyHelper.getTechnologyFromDB(bot, message, convo, luisUtil, nextThread, function (conversation, rows) {

            logUtil.debug("All Technologys to display in Convo: " + JSON.stringify(rows));

            if (rows.length === 0) {
                conversation.transitionTo(nextThread, t("ressource.askTechnologys.no_technologys_found"));
            } else {

                let qr = [];

                qr.push({title: t("ressource.askTechnologys.no_technologys_required"), payload: t("ressource.askTechnologys.no_technologys_required")})

                for (let i = 0; i < rows.length; i++) {

                    let oRow = rows[i];

                    if (oRow.code != null && oRow.value !== "") {
                        qr.push({title: oRow.value, payload: oRow.value})
                    }
                }

                //If there are 5 elements, offer to display more.
                if (rows.length === 5) {
                    qr.push({title: t("ressource.askTechnologys.display_more_technologys"), payload: t("ressource.askTechnologys.display_more_technologys")})
                }

                conversation.addQuestion({
                    text: t("ressource.askTechnologys.found_technologys"),
                    quick_replies: qr
                }, [
                    {
                        default: true,
                        callback: function (res, conversation) {

                            try {

                                if (res.text === t("ressource.askTechnologys.display_more_technologys")) {

                                    // //Add +5 to current offset, to display more results from db
                                    // conversation.setVar("offsetTechnology", conversation.vars.offsetTechnology + 5);
                                    //
                                    // this.displayTechnologys(bot, message, conversation, luisUtil, nextThread)

                                    conversation.transitionTo(threadName, t('nicht_implementiert'));

                                } else if (res.text === t("ressource.askTechnologys.no_technologys_required")) {

                                    //Set var in convo --> used afterwards to get search results form db
                                    conversation.setVar("ressourceTechnology", "None");

                                    //Reset offset
                                    conversation.setVar("offsetTechnology", 0);

                                    //continue to next thread
                                    if (nextThread !== "None") {
                                        conversation.gotoThread(nextThread);
                                    } else {
                                        conversation.next();
                                    }

                                } else {

                                    let aEntity = luisUtil.getEntityFromLuisResponse("ressourceTechnology", res);

                                    if (aEntity === null || aEntity === undefined || aEntity.length === 0 || aEntity === "") {
                                        // array empty or does not exist
                                        conversation.transitionTo(threadName, t('did_not_understand'));
                                    } else {

                                        //Set var in convo --> used afterwards to get search results form db
                                        conversation.setVar("ressourceTechnology", aEntity[0]);

                                        //Reset offset
                                        conversation.setVar("offsetTechnology", 0);
                                        logUtil.debug("ressourceTechnology = " + convo.vars.ressourceTechnology);

                                        //continue to next thread
                                        if (nextThread !== "None") {
                                            conversation.gotoThread(nextThread);
                                        } else {
                                            conversation.next();
                                        }

                                    }

                                }

                            } catch (err) {
                                errorUtil.displayErrorMessage(bot, message, err, false, false);
                            }

                        }
                    }
                ], {}, threadName);

            }
        }, convo.vars.limitTechnology, convo.vars.offsetTechnology);


    }

};
