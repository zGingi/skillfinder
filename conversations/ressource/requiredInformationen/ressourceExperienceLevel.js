module.exports = {
    displayExperienceLevels: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const experienceLevelHelper = require('../../../util/helper/experienceLevelHelper');
        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");

        experienceLevelHelper.getExperienceLevelFromDB(bot, message, convo, luisUtil, nextThread, function (conversation, rows) {

            logUtil.debug("All ExperienceLevels to display in Convo: " + JSON.stringify(rows));

            if (rows.length === 0) {
                conversation.transitionTo(nextThread, t("ressource.askExperienceLevels.no_experience_levels_found"));
            } else {

                let qr = [];

                qr.push({title: t("ressource.askExperienceLevels.no_experience_levels_required"), payload: t("ressource.askExperienceLevels.no_experience_levels_required")})

                for (let i = 0; i < rows.length; i++) {

                    let oRow = rows[i];

                    if (oRow.description != null && oRow.description !== "") {
                        qr.push({title: oRow.description, payload: oRow.description})
                    }
                }

                //If there are 5 elements, offer to display more.
                if (rows.length === 5) {
                    qr.push({title: t("ressource.askExperienceLevels.display_more_experience_level"), payload: t("ressource.askExperienceLevels.display_more_experience_levels")})
                }

                conversation.addQuestion({
                    text: t("ressource.askExperienceLevels.found_experience_levels"),
                    quick_replies: qr
                }, [
                    {
                        default: true,
                        callback: function (res, conversation) {

                            try {

                                if (res.text === t("ressource.askExperienceLevels.display_more_experience_levels")) {

                                    // //Add +5 to current offset, to display more results from db
                                    // conversation.setVar("offsetExperienceLevel", conversation.vars.offsetExperienceLevel + 5);
                                    //
                                    // this.displayExperienceLevels(bot, message, conversation, luisUtil, threadName, nextThread)

                                    conversation.transitionTo(threadName, t('nicht_implementiert'));

                                } else if (res.text === t("ressource.askExperienceLevels.no_experience_levels_required")) {

                                    //Set var in convo --> used afterwards to get search results form db
                                    conversation.setVar("ressourceExperienceLevel", "None");

                                    //Reset offset
                                    conversation.setVar("offsetExperienceLevel", 0);

                                    //continue to next thread
                                    if (nextThread !== "None") {
                                        conversation.gotoThread(nextThread);
                                    } else {
                                        conversation.next();
                                    }

                                } else {

                                    let aEntity = luisUtil.getEntityFromLuisResponse("ressourceExperienceLevel", res);

                                    if (aEntity === null || aEntity === undefined || aEntity.length === 0 || aEntity === "") {
                                        // array empty or does not exist
                                        conversation.transitionTo(threadName, t('did_not_understand'));
                                    } else {

                                        //Set var in convo --> used afterwards to get search results form db
                                        conversation.setVar("ressourceExperienceLevel", aEntity[0]);

                                        //Reset offset
                                        conversation.setVar("offsetExperienceLevel", 0);
                                        logUtil.debug("ressourceExperienceLevel = " + convo.vars.ressourceExperienceLevel);

                                        //continue to next thread
                                        if (nextThread !== "None") {
                                            conversation.gotoThread(nextThread);
                                        } else {
                                            conversation.next();
                                        }

                                    }

                                }

                            } catch (err) {
                                errorUtil.displayErrorMessage(bot, message, err, false, false);
                            }

                        }
                    }
                ], {}, threadName);

            }
        }, convo.vars.limitExperienceLevel, convo.vars.offsetExperienceLevel);


    }

};
