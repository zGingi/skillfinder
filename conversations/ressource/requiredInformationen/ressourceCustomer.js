module.exports = {
    displayCustomers: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const customerHelper = require('../../../util/helper/customerHelper');
        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");

        customerHelper.getCustomerFromDB(bot, message, convo, luisUtil, nextThread, function (conversation, rows) {

            logUtil.debug("All Customers to display in Convo: " + JSON.stringify(rows));

            if (rows.length === 0) {
                conversation.transitionTo(nextThread, t("ressource.askCustomers.no_customers_found"));
            } else {

                let qr = [];

                qr.push({title: t("ressource.askCustomers.no_customers_required"), payload: t("ressource.askCustomers.no_customers_required")})

                for (let i = 0; i < rows.length; i++) {

                    let oRow = rows[i];

                    if (oRow.code != null && oRow.value !== "") {
                        qr.push({title: oRow.value, payload: oRow.value})
                    }
                }

                //If there are 5 elements, offer to display more.
                if (rows.length === 5) {
                    qr.push({title: t("ressource.askCustomers.display_more_customers"), payload: t("ressource.askCustomers.display_more_customers")})
                }

                conversation.addQuestion({
                    text: t("ressource.askCustomers.found_customers"),
                    quick_replies: qr
                }, [
                    {
                        default: true,
                        callback: function (res, conversation) {

                            try {

                                if (res.text === t("ressource.askCustomers.display_more_customers")) {

                                    // //Add +5 to current offset, to display more results from db
                                    // conversation.setVar("offsetCustomer", conversation.vars.offsetCustomer + 5);
                                    //
                                    // this.displayCustomers(bot, message, conversation, luisUtil, nextThread)
                                    conversation.transitionTo(threadName, t('nicht_implementiert'));

                                } else if (res.text === t("ressource.askCustomers.no_customers_required")) {

                                    //Set var in convo --> used afterwards to get search results form db
                                    conversation.setVar("ressourceCustomer", "None");

                                    //Reset offset
                                    conversation.setVar("offsetCustomer", 0);

                                    //continue to next thread
                                    if (nextThread !== "None") {
                                        conversation.gotoThread(nextThread);
                                    } else {
                                        conversation.next();
                                    }

                                } else {

                                    let aEntity = luisUtil.getEntityFromLuisResponse("ressourceCustomer", res);

                                    if (aEntity === null || aEntity === undefined || aEntity.length === 0 || aEntity === "") {
                                        // array empty or does not exist
                                        conversation.transitionTo(threadName, t('did_not_understand'));
                                    } else {

                                        //Set var in convo --> used afterwards to get search results form db
                                        conversation.setVar("ressourceCustomer", aEntity[0]);

                                        //Reset offset
                                        conversation.setVar("offsetCustomer", 0);
                                        logUtil.debug("ressourceCustomer = " + convo.vars.ressourceCustomer);

                                        //continue to next thread
                                        if (nextThread !== "None") {
                                            conversation.gotoThread(nextThread);
                                        } else {
                                            conversation.next();
                                        }

                                    }

                                }

                            } catch (err) {
                                errorUtil.displayErrorMessage(bot, message, err, false, false);
                            }

                        }
                    }
                ], {}, threadName);

            }
        }, convo.vars.limitCustomer, convo.vars.offsetCustomer);


    }

};
