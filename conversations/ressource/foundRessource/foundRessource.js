module.exports = {
    displayFoundRessources: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const employeeHelper = require('../../../util/helper/employeeHelper');
        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");
        const foundRessources = this;

        employeeHelper.getRessourcesFromDB(bot, message, convo, luisUtil, nextThread, function (conversation, rows) {

            logUtil.debug("All Employees to display in Convo: " + JSON.stringify(rows));

            if (rows.length === 0) {
                conversation.transitionTo(nextThread, t("ressource.foundEmployees.no_employees_found"));
            } else {

                let qr = [];

                for (let i = 0; i < rows.length; i++) {

                    let oRow = rows[i];

                    if (oRow.firstname != null && oRow.firstname !== "") {

                        let name = oRow.firstname + " " + oRow.lastname;
                        let id = oRow.id + "";
                        qr.push({title: name, payload: id})
                    }
                }

                //If there are 3 elements, offer to display more.
                if (rows.length === parseInt(convo.vars.limitEmployee) ) {
                    qr.push({title: t("ressource.foundEmployees.display_more_employees"), payload: t("ressource.foundEmployees.display_more_employees")})
                }

                qr.push({title: t("ressource.foundEmployees.no_employees_required"), payload: t("ressource.foundEmployees.no_employees_required")});

                conversation.addQuestion({
                // conversation.ask({
                    text: t("ressource.foundEmployees.found_employees"),
                    quick_replies: qr
                }, [
                    {
                        default: true,
                        callback: function (res, conversation) {

                            try {

                                if (res.text === t("ressource.foundEmployees.display_more_employees")) {

                                    // //Add +5 to current offset, to display more results from db
                                    // conversation.setVar("offsetEmployee", conversation.vars.offsetEmployee + 3);
                                    //
                                    // this.displayFoundRessources(bot, message, conversation, luisUtil, nextThread)

                                    conversation.transitionTo(threadName, t('nicht_implementiert'));

                                } else if (res.text === t("ressource.foundEmployees.no_employees_required")) {

                                    //Set var in convo --> used afterwards to get search results form db
                                    conversation.setVar("ressourcesEmployee", "None");

                                    //Reset offset
                                    conversation.setVar("offsetEmployee", 0);

                                    //continue to next thread
                                    conversation.gotoThread("correctRequiredInfromation");

                                } else {

                                    logUtil.debug("Selected Employee: " + res.text);

                                    conversation.setVar("ressourcesEmployee", res.text);

                                    foundRessources.displayFoundEmployee(bot, message, convo, luisUtil, "displayFoundEmployee", "None");

                                    // continue to next thread
                                    if (nextThread !== "None") {
                                        conversation.gotoThread(nextThread);
                                    } else {
                                        conversation.next();
                                    }

                                }

                            } catch (err) {
                                errorUtil.displayErrorMessage(bot, message, err, false, false);
                            }

                        }
                    }
                ], {}, threadName);
                // ]);

                

            }
        }, convo.vars.limitEmployee, convo.vars.offsetEmployee);


    },

    displayFoundEmployee: function (bot, message, convo, luisUtil, threadName, nextThread = "None") {

        const employeeHelper = require('../../../util/helper/employeeHelper');
        const {t} = require('localizify');
        const logUtil = require("../../../util/logUtil");
        const errorUtil = require("../../../util/errorUtil");

        convo.addQuestion({
            text: "Do you want to select this Employee?",
            quick_replies: [
                {
                    title: t('yes'),
                    payload: t('yes'),
                },
                {
                    title: t('no'),
                    payload: t('no'),
                },
            ]
        }, [
            {
                default: true,
                callback: function (res, convo) {
                    try {
                        switch (res.text) {

                            case t('yes'):
                                convo.next();
                                break;
                            case t('no'):
                                convo.gotoThread("correctRequiredInfromation");
                                break;
                            default:
                                convo.say(t('did_not_understand'));
                                convo.repeat();
                                break;
                        }
                    } catch (err) {
                        errorUtil.displayErrorMessage(bot, message, err, false, false);
                    }

                }
            }
        ], {}, threadName);

        employeeHelper.getEmployeeWithId(bot, message, convo, luisUtil, nextThread, function (conversation, rows) {

            if (rows.length === 0) {
                conversation.transitionTo(nextThread, t("ressource.askCustomers.no_customers_found"));
            } else {

                logUtil.debug("Found single employee to display: " + JSON.stringify(rows[0]));
                let employee = rows[0];

                let qr = [];

                qr.push({
                    title: t("ressource.singleEmployee.contact_mail"),
                    payload: t("ressource.singleEmployee.contact_mail")
                });
                qr.push({
                    title: t("ressource.singleEmployee.contact_phone"),
                    payload: t("ressource.singleEmployee.contact_phone")
                });
                qr.push({
                    title: t("ressource.singleEmployee.new_search"),
                    payload: t("ressource.singleEmployee.new_search")
                });

                let sEmployeeInformation = "<br>";

                if (employee["jobtitle"] !== "" && employee["jobtitle"] !== undefined) {
                    sEmployeeInformation += employee["jobtitle"] + " ";
                }

                if (employee["firstname"] !== "" && employee["lastname"] !== "" && employee["firstname"] !== undefined && employee["lastname"] !== undefined) {
                    sEmployeeInformation += employee["firstname"] + " " + employee["lastname"] + ".<br><br>";
                }

                if (employee["experiencelevel"] !== "" && employee["experiencelevel"] !== undefined) {
                    sEmployeeInformation += employee["experiencelevel"] + " at herAkles AG" + ".<br><br>";
                }

                if (employee["streetnumber"] !== "" && employee["streetnumber"] !== undefined
                        && employee["street"] !== "" && employee["street"] !== undefined) {
                    sEmployeeInformation += "Address: <br>" + employee["street"] + " " + employee["streetnumber"] + "<br>";
                }

                if (employee["addressline2"] !== "" && employee["addressline2"] !== undefined) {
                    sEmployeeInformation += employee["addressline2"] + "<br> ";
                }

                if (employee["addressline4"] !== "" && employee["addressline4"] !== undefined) {
                    sEmployeeInformation += employee["addressline4"] + "<br>";
                }

                if (employee["city"] !== "" && employee["city"] !== undefined) {
                    sEmployeeInformation += employee["city"] + ", ";
                }

                if (employee["zipcode"] !== "" && employee["zipcode"] !== undefined) {
                    sEmployeeInformation += employee["zipcode"] + "<br>";
                }

                if (employee["country"] !== "" && employee["country"] !== undefined) {
                    sEmployeeInformation += employee["country"] + " ";
                }

                if (employee["countryIsoCode"] !== "" && employee["countryIsoCode"] !== undefined) {
                    sEmployeeInformation += "(" + employee["countryIsoCode"] + ") ";
                }

                conversation.addQuestion({
                    // conversation.ask({
                    text: t("ressource.singleEmployee.found_employee", {
                        employeeInformation: sEmployeeInformation
                    }),
                    quick_replies: qr
                }, [
                    {
                        default: true,
                        callback: function (res, conversation) {

                            try {

                                switch (res.text) {

                                    case t("ressource.singleEmployee.new_search"):

                                        //Reset all Variables required to search for a ressource
                                        let aVars = ["ressourceSkill", "ressourceTechnology", "ressourceProject", "ressourceCustomer", "ressourceExperienceLevel", "ressourceExperienceYears"];
                                        for (var x = 0; x < aVars.length; x++) {
                                            logUtil.debug("Initialize var " + aVars[x] + " with value to 'None'");
                                            convo.setVar(aVars[x], "None");
                                        }

                                        //Reset all Offset for DB results
                                        convo.setVar("offsetSkill", 0);
                                        convo.setVar("offsetTechnology", 0);
                                        convo.setVar("offsetProject", 0);
                                        convo.setVar("offsetCustomer", 0);
                                        convo.setVar("offsetExperienceLevel", 0);
                                        convo.setVar("offsetEmployee", 0);

                                        //Set var in convo --> used afterwards to get search results form db
                                        conversation.setVar("ressourceCustomer", "None");

                                        //continue to next thread
                                        conversation.transitionTo("initialMessage", "Reseting all search parameters. Starting search from beginning");

                                        break;

                                    case t("ressource.singleEmployee.contact_phone"):

                                        //continue to next thread
                                        conversation.transitionTo("convoEnd", "Perfect, you can call the Emploxyee with the following Link: <a href=\"tel:+41791234567\">Call +41 (0)79 123 45 67</a>");

                                        break;

                                    case t("ressource.singleEmployee.contact_mail"):

                                        let reciever= "helena.muster@herakles.com";
                                        let subject= "Project%20Orion%20-%20Resource%20Planing";
                                        let body= "Dear%20Helena%0A%0AI%20would%20require%20your%20assistance%20in%20the%20following%20Project%3A%0A%0AProject%20Name%3A%20Orion%0ACustomer%3A%20OLYMP%20Corporation%0ATechnology%3A%20SAP%0ASkills%3A%20SAP%20FI%2FCO%20Module%0A%0ACould%20you%20please%20inform%20me%2C%20if%20you%20have%20time%20for%20this%20task.%0A%0AKind%20regards%0APeter%0AProjectmanager%0AherAkles%20AG%0Awww.herakles.com";

                                        //continue to next thread
                                        conversation.transitionTo("convoEnd", "Perfect, you can contact the Emploxyee over Mail here: <a href=\"mailto:" + reciever + "?Subject=" + subject + "&Body=" + body + "\" target=\"_top\">Send Mail</a>");

                                        break;

                                    default:
                                        conversation.transitionTo("convoEnd", "Reseting all search parameters. Starting search from beginning");
                                        break;

                                }

                            } catch (err) {
                                errorUtil.displayErrorMessage(bot, message, err, false, false);
                            }

                        }
                    }
                    // ]);
                ], {}, threadName);
            }
        });

    }

};
