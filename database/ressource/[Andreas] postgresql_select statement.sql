SELECT DISTINCT employee.id                                                                 as id,
                employee.firstname                                                          as firstname,
                employee.lastname                                                           as lastname,
                j.value                                                                     as jobTitle,
                t2.value                                                                    as title,
                a.streetnumber                                                              as streetnumber,
                a.addressline2                                                              as addressline2,
                a.addressline3                                                              as addressline3,
                a.addressline4                                                              as addressline4,
                c2.value                                                                    as city,
                c2.zipcode                                                                  as zipcode,
                c3.value                                                                    as country,
                c3.isocode                                                                  as countryIsoCode,
                s.code                                                                      as skillCode,
                s.value                                                                     as skillValue,
                t.code                                                                      as technologyCode,
                t.value                                                                     as technologyValue,
                e.start_date                                                                as experienceStartDate,
                e.end_date                                                                  as experienceEndDate,
                DATE_PART('year', e.end_date::date) - DATE_PART('year', e.start_date::date) as experienceYears,
                e.description                                                               as experienceLevel,
                p.code                                                                      as projectCode,
                p.value                                                                     as projectValue,
                c.code                                                                      as customerCode,
                c.value                                                                     as customerValue
FROM employee
         LEFT JOIN employee_skill es on employee.id = es.employeeid
         LEFT JOIN skill s on es.skillid = s.id
         LEFT JOIN skill_technology st on s.id = st.skillid
         LEFT JOIN technology t on st.technologyid = t.id
         LEFT JOIN employee_project ep on employee.id = ep.employeeid
         LEFT JOIN project p on ep.projectid = p.id
         LEFT JOIN customer_project cp on p.id = cp.projectid
         LEFT JOIN customer c on cp.customerid = c.id
         LEFT JOIN experiencelevel e on employee.experiencelevelid = e.id
         LEFT JOIN title t2 on employee.titleid = t2.id
         LEFT JOIN jobtitle j on employee.jobtitleid = j.id
         LEFT JOIN address a on employee.addressid = a.id
         LEFT JOIN city c2 on a.cityid = c2.id
         LEFT JOIN country c3 on c2.id = c3.id
WHERE employee.id IS NOT NULL