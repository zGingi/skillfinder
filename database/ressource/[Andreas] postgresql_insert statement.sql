-- -----------------------------------------------------
-- Insert Table skillfinder.Skill
-- -----------------------------------------------------

INSERT INTO Skill (Code, Value, Description)
VALUES (1, 'Skill 1', 'My fancy description');

INSERT INTO Skill (Code, Value, Description)
VALUES (2, 'Skill 2', 'My fancy description');

INSERT INTO Skill (Code, Value, Description)
VALUES (3, 'Skill 3', 'My fancy description');

INSERT INTO Skill (Code, Value, Description)
VALUES (4, 'Skill 4', 'My fancy description');

INSERT INTO Skill (Code, Value, Description)
VALUES (5, 'Skill 5', 'My fancy description');

INSERT INTO Skill (Code, Value, Description)
VALUES (6, 'Skill 6', 'My fancy description');


-- -----------------------------------------------------
-- Insert Table skillfinder.Country
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Insert Table skillfinder.City
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Insert Table skillfinder.Address
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Insert Table skillfinder.Customer
-- -----------------------------------------------------

INSERT INTO customer (addressid, delegateid, code, value, firstname, lastname, phone, url, email)
VALUES (NULL, NULL, 1, 'Customer 1', 'Firstname 1', 'Lastname 1', '+41791234567', 'www.google.com', 'mail@mail.com')

INSERT INTO customer (addressid, delegateid, code, value, firstname, lastname, phone, url, email)
VALUES (NULL, NULL, 2, 'Customer 2', 'Firstname 2', 'Lastname 2', '+41791234567', 'www.google.com', 'mail@mail.com');

INSERT INTO customer (addressid, delegateid, code, value, firstname, lastname, phone, url, email)
VALUES (NULL, NULL, 3, 'Customer 3', 'Firstname 3', 'Lastname 3', '+41791234567', 'www.google.com', 'mail@mail.com');

INSERT INTO customer (addressid, delegateid, code, value, firstname, lastname, phone, url, email)
VALUES (NULL, NULL, 4, 'Customer 4', 'Firstname 4', 'Lastname 4', '+41791234567', 'www.google.com', 'mail@mail.com');

INSERT INTO customer (addressid, delegateid, code, value, firstname, lastname, phone, url, email)
VALUES (NULL, NULL, 5, 'Customer 5', 'Firstname 5', 'Lastname 5', '+41791234567', 'www.google.com', 'mail@mail.com');

INSERT INTO customer (addressid, delegateid, code, value, firstname, lastname, phone, url, email)
VALUES (NULL, NULL, 6, 'Customer 6', 'Firstname 6', 'Lastname 6', '+41791234567', 'www.google.com', 'mail@mail.com');

-- -----------------------------------------------------
-- Insert Table skillfinder.Project
-- -----------------------------------------------------

INSERT INTO project (Code, Value, Description)
VALUES (1, 'Project 1', 'My fancy description');

INSERT INTO project (Code, Value, Description)
VALUES (2, 'Project 2', 'My fancy description');

INSERT INTO project (Code, Value, Description)
VALUES (3, 'Project 3', 'My fancy description');

INSERT INTO project (Code, Value, Description)
VALUES (4, 'Project 4', 'My fancy description');

INSERT INTO project (Code, Value, Description)
VALUES (5, 'Project 5', 'My fancy description');

INSERT INTO project (Code, Value, Description)
VALUES (6, 'Project 6', 'My fancy description');

-- -----------------------------------------------------
-- Insert Table skillfinder.Jobtitle
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Insert Table skillfinder.Experiencelevel
-- -----------------------------------------------------

INSERT INTO experiencelevel (description, start_date, end_date)
VALUES ('Senior' , '01.01.2000', '01.01.2019');

INSERT INTO experiencelevel (description, start_date, end_date)
VALUES ('Professional' , '01.01.1999', '01.01.2000');

INSERT INTO experiencelevel (description, start_date, end_date)
VALUES ('Junior' , '01.01.1990', '01.01.1993');

-- -----------------------------------------------------
-- Insert Table skillfinder.Title
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Insert Table skillfinder.Employee
-- -----------------------------------------------------

INSERT INTO employee (firstname, lastname, start_date, end_date, experiencelevelid, jobtitleid, addressid, titleid, managerid)
VALUES ('Firstname 1', 'Lastname 1', NULL, NULL, 1, NULL, NULL, NULL, NULL);

-- -----------------------------------------------------
-- Insert Table skillfinder.Employee_Skill
-- -----------------------------------------------------

INSERT INTO employee_skill (skillid, employeeid)
VALUES (1,1);

-- -----------------------------------------------------
-- Insert Table skillfinder.Customer_Project
-- -----------------------------------------------------

INSERT INTO customer_project (projectid, customerid)
VALUES (1,1);

-- -----------------------------------------------------
-- Insert Table skillfinder.Employee_Project
-- -----------------------------------------------------

INTO employee_project (projectid, employeeid)
VALUES (1,1);

-- -----------------------------------------------------
-- Insert Table skillfinder.Technology
-- -----------------------------------------------------

INSERT INTO technology (Code, Value, Description)
VALUES (1, 'Technology 1', 'My fancy description');

INSERT INTO technology (Code, Value, Description)
VALUES (2, 'Technology 2', 'My fancy description');

INSERT INTO technology (Code, Value, Description)
VALUES (3, 'Technology 3', 'My fancy description');

INSERT INTO technology (Code, Value, Description)
VALUES (4, 'Technology 4', 'My fancy description');

INSERT INTO technology (Code, Value, Description)
VALUES (5, 'Technology 5', 'My fancy description');

INSERT INTO technology (Code, Value, Description)
VALUES (6, 'Technology 6', 'My fancy description');

-- -----------------------------------------------------
-- Insert Table skillfinder.Project_Technology
-- -----------------------------------------------------

INSERT INTO project_technology (projectid, technologyid)
VALUES (1,1);

-- -----------------------------------------------------
-- Insert Table skillfinder.Skill_Technology
-- -----------------------------------------------------

INSERT INTO skill_technology (skillid, technologyid)
VALUES (1,1);