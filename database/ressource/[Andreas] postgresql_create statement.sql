-- -----------------------------------------------------
-- Table skillfinderSkill
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Skill (
  Id SERIAL NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Description VARCHAR(500) NULL,
  PRIMARY KEY (Id));

ALTER TABLE Skill
    ADD UNIQUE (Code);

-- -----------------------------------------------------
-- Table skillfinderCountry
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Country (
  Id SERIAL NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Description VARCHAR(500) NULL,
  Isocode VARCHAR(3) NULL,
  PRIMARY KEY (Id));

  ALTER TABLE Country
    ADD UNIQUE (Code);


-- -----------------------------------------------------
-- Table skillfinderCity
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS City (
  Id SERIAL NOT NULL,
  Countryid INT NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Zipcode VARCHAR(10) NULL,
  Description VARCHAR(500) NULL,
  PRIMARY KEY (Id));

  ALTER TABLE City
    ADD FOREIGN KEY (Countryid)
    REFERENCES City(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

  ALTER TABLE City
    ADD UNIQUE (Code);


-- -----------------------------------------------------
-- Table skillfinderAddress
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Address (
  Id SERIAL NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Cityid INT NOT NULL,
  Street VARCHAR(500) NULL,
  Streetnumber VARCHAR(45) NULL,
  Addressline2 VARCHAR(45) NULL,
  Addressline3 VARCHAR(45) NULL,
  Addressline4 VARCHAR(45) NULL,
  PRIMARY KEY (Id));

      ALTER TABLE Address
    ADD UNIQUE (Code);

    ALTER TABLE Address
    ADD FOREIGN KEY (Cityid)
    REFERENCES City(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table skillfinderCustomer
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Customer (
  Id SERIAL NOT NULL,
  Addressid INT NULL,
  Delegateid INT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Firstname INT NULL,
  Lastname VARCHAR(45) NULL,
  Phone VARCHAR(12) NULL,
  Url VARCHAR(2083) NULL,
  Email VARCHAR(254) NULL,
  PRIMARY KEY (Id));

      ALTER TABLE Customer
    ADD UNIQUE (Code);

    ALTER TABLE Customer
    ADD FOREIGN KEY (Addressid)
    REFERENCES Address(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

     ALTER TABLE Customer
    ADD FOREIGN KEY (Delegateid)
    REFERENCES Customer(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table skillfinderProject
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Project (
  Id SERIAL NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Description VARCHAR(500) NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Project
    ADD UNIQUE (Code);


-- -----------------------------------------------------
-- Table skillfinderJobtitle
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Jobtitle (
  Id SERIAL NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Description VARCHAR(500) NULL,
  PRIMARY KEY (Id));

  ALTER TABLE Jobtitle
    ADD UNIQUE (Code);

-- -----------------------------------------------------
-- Table skillfinderExperiencelevel
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Experiencelevel (
  Id SERIAL NOT NULL,
  Description VARCHAR(500) NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  PRIMARY KEY (Id));


-- -----------------------------------------------------
-- Table skillfinderTitle
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Title (
  Id SERIAL NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Description VARCHAR(500) NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Country
    ADD UNIQUE (Code);


-- -----------------------------------------------------
-- Table skillfinderEmployee
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Employee (
  Id SERIAL NOT NULL,
  Firstname VARCHAR(45) NULL,
  Lastname VARCHAR(45) NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  Experiencelevelid INT NULL,
  Jobtitleid INT NULL,
  Addressid INT NULL,
  Titleid INT NULL,
  Managerid INT NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Employee
    ADD FOREIGN KEY (Experiencelevelid)
    REFERENCES Experiencelevel(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Employee
    ADD FOREIGN KEY (Jobtitleid)
    REFERENCES Jobtitle(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Employee
    ADD FOREIGN KEY (Addressid)
    REFERENCES Address(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Employee
    ADD FOREIGN KEY (Titleid)
    REFERENCES Title(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Employee
    ADD FOREIGN KEY (Managerid)
    REFERENCES Employee(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table skillfinderEmployee_Skill
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Employee_Skill (
  Id SERIAL NOT NULL,
  Employeeid INT NOT NULL,
  Skillid INT NOT NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Employee_Skill
    ADD FOREIGN KEY (Employeeid)
    REFERENCES Employee(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Employee_Skill
    ADD FOREIGN KEY (Skillid)
    REFERENCES Skill(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table skillfinderCustomer_Project
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Customer_Project (
  Id SERIAL NOT NULL,
  Projectid INT NOT NULL,
  Customerid INT NOT NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Customer_Project
    ADD FOREIGN KEY (Projectid)
    REFERENCES Project(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Customer_Project
    ADD FOREIGN KEY (Customerid)
    REFERENCES Customer(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table skillfinderEmployee_Project
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Employee_Project (
  Id SERIAL NOT NULL,
  Employeeid INT NOT NULL,
  Projectid INT NOT NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  PRIMARY KEY (Id));

ALTER TABLE Employee_Project
    ADD FOREIGN KEY (Employeeid)
    REFERENCES Employee(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


ALTER TABLE Employee_Project
    ADD FOREIGN KEY (Projectid)
    REFERENCES Project(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table skillfinderTechnology
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Technology (
  Id SERIAL NOT NULL,
  Code INT NOT NULL,
  Value VARCHAR(45) NOT NULL,
  Description VARCHAR(500) NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Technology
    ADD UNIQUE (Code);


-- -----------------------------------------------------
-- Table skillfinderProject_Technology
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Project_Technology (
  Id SERIAL NOT NULL,
  Projectid INT NOT NULL,
  Technologyid INT NOT NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Project_Technology
    ADD FOREIGN KEY (Projectid)
    REFERENCES Project(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Project_Technology
    ADD FOREIGN KEY (Technologyid)
    REFERENCES Technology(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- -----------------------------------------------------
-- Table skillfinderSkill_Technology
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Skill_Technology (
  Id SERIAL NOT NULL,
  Skillid INT NOT NULL,
  Technologyid INT NOT NULL,
  Start_Date DATE NULL,
  End_Date DATE NULL,
  PRIMARY KEY (Id));

    ALTER TABLE Skill_Technology
    ADD FOREIGN KEY (Skillid)
    REFERENCES Skill(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE Skill_Technology
    ADD FOREIGN KEY (Technologyid)
    REFERENCES Technology(Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;