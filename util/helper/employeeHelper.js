module.exports = {

    getRessourcesFromDB: function(bot, message, convo, luisUtil, nextThread, returnDbEntries, limit = 3, offset = 0){

        const pgUtil = require("../pgUtil");
        const logUtil = require("../logUtil");

        let ressourceSkill = "";
        if(convo.vars.ressourceSkill !== "None" && convo.vars.ressourceSkill !== ""){
            ressourceSkill = " AND s.value = '" + convo.vars.ressourceSkill + "'";
        }
        let ressourceTechnology = "";
        if(convo.vars.ressourceTechnology !== "None" && convo.vars.ressourceTechnology !== ""){
            ressourceTechnology = " AND t.value = '" + convo.vars.ressourceTechnology + "'";
        }
        let ressourceProject = "";
        if(convo.vars.ressourceProject !== "None" && convo.vars.ressourceProject !== ""){
            ressourceProject = " AND p.value = '" + convo.vars.ressourceProject + "'";
        }
        let ressourceCustomer = "";
        if(convo.vars.ressourceCustomer !== "None" && convo.vars.ressourceCustomer !== ""){
            ressourceCustomer = " AND c.value = '" + convo.vars.ressourceCustomer + "'";
        }
        let ressourceExperienceLevel = "";
        if(convo.vars.ressourceExperienceLevel !== "None" && convo.vars.ressourceExperienceLevel !== ""){
            ressourceExperienceLevel = " AND e.description = '" + convo.vars.ressourceExperienceLevel + "'";
        }

        let ressourceExperienceYears = "";
        if(convo.vars.ressourceExperienceYears !== "None" && convo.vars.ressourceExperienceYears !== ""){

            switch(convo.vars.ressourceExperienceYears){

                case 1:
                    ressourceExperienceYears = " AND DATE_PART('year', e.end_date::date) - DATE_PART('year', e.start_date::date) <= 1 ";
                    break;
                case 5:
                    ressourceExperienceYears = " AND DATE_PART('year', e.end_date::date) - DATE_PART('year', e.start_date::date) >= 1 AND DATE_PART('year', e.end_date::date) - DATE_PART('year', e.start_date::date) <= 5";
                    break;
                default:
                    ressourceExperienceYears = " AND DATE_PART('year', e.end_date::date) - DATE_PART('year', e.start_date::date) > 5 ";
                    break;

            }
        }


        let pgQuery = " SELECT DISTINCT employee.id                                                                 as id,";
        pgQuery +=    "                 employee.firstname                                                          as firstname,";
        pgQuery +=    "                 employee.lastname                                                           as lastname,";
        pgQuery +=    "                 j.value                                                                     as jobTitle,";
        pgQuery +=    "                 t2.value                                                                    as title,";
        pgQuery +=    "                 a.street                                                                    as street,";
        pgQuery +=    "                 a.streetnumber                                                              as streetnumber,";
        pgQuery +=    "                 a.addressline2                                                              as addressline2,";
        pgQuery +=    "                 a.addressline3                                                              as addressline3,";
        pgQuery +=    "                 a.addressline4                                                              as addressline4,";
        pgQuery +=    "                 c2.value                                                                    as city,";
        pgQuery +=    "                 c2.zipcode                                                                  as zipcode,";
        pgQuery +=    "                 c3.value                                                                    as country,";
        pgQuery +=    "                 c3.isocode                                                                  as countryIsoCode,";
        pgQuery +=    "                 s.code                                                                      as skillCode,";
        pgQuery +=    "                 s.value                                                                     as skillValue,";
        pgQuery +=    "                 t.code                                                                      as technologyCode,";
        pgQuery +=    "                 t.value                                                                     as technologyValue,";
        pgQuery +=    "                 e.start_date                                                                as experienceStartDate,";
        pgQuery +=    "                 e.end_date                                                                  as experienceEndDate,";
        pgQuery +=    "                 DATE_PART('year', e.end_date::date) - DATE_PART('year', e.start_date::date) as experienceYears,";
        pgQuery +=    "                 e.description                                                               as experienceLevel,";
        pgQuery +=    "                 p.code                                                                      as projectCode,";
        pgQuery +=    "                 p.value                                                                     as projectValue,";
        pgQuery +=    "                 c.code                                                                      as customerCode,";
        pgQuery +=    "                 c.value                                                                     as customerValue";
        pgQuery +=    " FROM employee";
        pgQuery +=    "          LEFT JOIN employee_skill es on employee.id = es.employeeid";
        pgQuery +=    "          LEFT JOIN skill s on es.skillid = s.id";
        pgQuery +=    "          LEFT JOIN skill_technology st on s.id = st.skillid";
        pgQuery +=    "          LEFT JOIN technology t on st.technologyid = t.id";
        pgQuery +=    "          LEFT JOIN employee_project ep on employee.id = ep.employeeid";
        pgQuery +=    "          LEFT JOIN project p on ep.projectid = p.id";
        pgQuery +=    "          LEFT JOIN customer_project cp on p.id = cp.projectid";
        pgQuery +=    "          LEFT JOIN customer c on cp.customerid = c.id";
        pgQuery +=    "          LEFT JOIN experiencelevel e on employee.experiencelevelid = e.id";
        pgQuery +=    "          LEFT JOIN title t2 on employee.titleid = t2.id";
        pgQuery +=    "          LEFT JOIN jobtitle j on employee.jobtitleid = j.id";
        pgQuery +=    "          LEFT JOIN address a on employee.addressid = a.id";
        pgQuery +=    "          LEFT JOIN city c2 on a.cityid = c2.id";
        pgQuery +=    "          LEFT JOIN country c3 on c2.id = c3.id";
        pgQuery +=    " WHERE employee.id IS NOT NULL";
        pgQuery +=    ressourceSkill ;
        pgQuery +=    ressourceTechnology ;
        pgQuery +=    ressourceProject ;
        pgQuery +=    ressourceCustomer ;
        pgQuery +=    ressourceExperienceLevel ;
        pgQuery +=    ressourceExperienceYears ;
        pgQuery +=    " LIMIT " + limit + " OFFSET " + offset;
        logUtil.debug("Employees as Ressources Query: " + pgQuery);

        //Connect to DB
        /////////////////////////////
        const pgClient = pgUtil.getDB();
        pgClient.connect();



        // Execute Query and return res
        /////////////////////////////
        pgClient.query(pgQuery,
            (err, res) => {
                if (err) throw new Error(err.stack);

                pgClient.end();

                logUtil.debug("Employees as Ressources DB - Response: " + JSON.stringify(res));

                if (res.rows.length > 0) {
                    logUtil.debug("Employees as Ressources to display from : " + JSON.stringify(res.rows));
                    returnDbEntries(convo, res.rows);
                } else {
                    returnDbEntries(convo, "");
                }

            });
    },

    getEmployeeWithId(bot, message, convo, luisUtil, nextThread, returnDbEntries){

        const pgUtil = require("../pgUtil");
        const logUtil = require("../logUtil");

        let pgQuery = "SELECT * FROM employee WHERE id = " + convo.vars.ressourcesEmployee;
        logUtil.debug("Employee Query: " + pgQuery);

        //Connect to DB
        /////////////////////////////
        const pgClient = pgUtil.getDB();
        pgClient.connect();

        // Execute Query and return res
        /////////////////////////////
        pgClient.query(pgQuery,
            (err, res) => {
                if (err) throw new Error(err.stack);

                pgClient.end();

                logUtil.debug("Employee DB - Response: " + JSON.stringify(res));

                if (res.rows.length > 0) {
                    logUtil.debug("Employee to display from : " + JSON.stringify(res.rows));
                    returnDbEntries(convo, res.rows);
                } else {
                    returnDbEntries(convo, "");
                }

            });

    }


};