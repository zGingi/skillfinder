module.exports = {

    getSkillsFromDB: function(bot, message, convo, luisUtil, nextThread, returnDbEntries, limit = 5, offset = 0){

        const pgUtil = require("../pgUtil");
        const logUtil = require("../logUtil");

        let pgQuery = "SELECT * FROM skill LIMIT " + limit + " OFFSET " + offset;
        logUtil.debug("Skills Query: " + pgQuery);

        //Connect to DB
        /////////////////////////////
        const pgClient = pgUtil.getDB();
        pgClient.connect();

        // Execute Query and return res
        /////////////////////////////
        pgClient.query(pgQuery,
            (err, res) => {
                if (err) throw new Error(err.stack);

                pgClient.end();

                logUtil.debug("Skills DB - Response: " + JSON.stringify(res));

                if (res.rows.length > 0) {
                    logUtil.debug("Skills to display from : " + JSON.stringify(res.rows));
                    returnDbEntries(convo, res.rows);
                } else {
                    returnDbEntries(convo, "");
                }

            });
    }


};